﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shieldChargeBar : MonoBehaviour
{
    GameObject ship;

    void Start()
    {
        ship = GameObject.FindWithTag("ship");
        transform.localScale = new Vector3(0, transform.localScale.y, transform.localScale.z);
    }

    void Update()
    {
        float x = Mathf.Lerp(0, 3.55f, ship.GetComponent<ship>().shieldLevels);
        transform.localScale = new Vector3(x, transform.localScale.y, transform.localScale.z);
    }
}
