﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class lasers : MonoBehaviour
{
    GameObject ship;

    void Start()
    {
        ship = GameObject.FindWithTag("ship");
        gameObject.GetComponent<Button>().onClick.AddListener(ClickTask);
    }

    void ClickTask()
    {
        ship.GetComponent<ship>().FireLaser();
    }
}
