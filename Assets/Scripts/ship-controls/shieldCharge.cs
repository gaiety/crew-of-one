﻿using System.Collections;
using UnityEngine;

public class shieldCharge : MonoBehaviour
{
    GameObject ship;
    bool down = false;

    void Start()
    {
        ship = GameObject.FindWithTag("ship");
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0)) down = true;
        if (Input.GetMouseButtonUp(0)) down = false;
    }

    void OnMouseExit()
    {
        down = false;
    }

    void Update()
    {
        if (down) ship.GetComponent<ship>().ChargeShields();
    }
}
