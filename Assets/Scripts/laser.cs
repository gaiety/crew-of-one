﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laser : MonoBehaviour
{
    public float destroyWhenY = 10.0f;
    public float speed = 5.0f;
    Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (!shouldDestroySelf()) moveUp();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "asteroid")
        {
            collider.gameObject.SendMessage("laserHit");
            Destroy(gameObject);
        }
    }

    bool shouldDestroySelf()
    {
        if (transform.position.y >= destroyWhenY)
        {
            Destroy(gameObject);
            return true;
        }
        return false;
    }

    void moveUp()
    {
        Vector3 tempVect = new Vector3(0, 1, 0);
        tempVect = tempVect * speed * Time.deltaTime;
        rb.MovePosition(transform.position + tempVect);
    }
}
