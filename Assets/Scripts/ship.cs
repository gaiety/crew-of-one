﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine;

public class ship : MonoBehaviour
{
    public float health = 1; // 1-0
    public float shieldLevels = 0; // 1-0
    public float shieldLossOverTime = 0.01f;
    public float shieldProtectionPercentage = 0.5f;
    public float movementSpeed = 5;
    public Transform laserPrefab;
    public Transform explosionPrefab;
    Rigidbody2D rb;
    GameObject shield;
    Color originalShieldColor;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        shield = GameObject.FindWithTag("shield");
        originalShieldColor = shield.GetComponent<SpriteRenderer>().color;
    }

    void Update()
    {
        if (health < 0) health = 0;
        UpdateShields();
    }

    void TakeDamage(float damage)
    {
        health -= damage * (shieldLevels > 0 ? shieldProtectionPercentage : 1);
        if (health <= 0) BlogUp();
    }

    public void AftThrusters(float h)
    {
        Vector3 tempVect = new Vector3(h, 0, 0);
        tempVect = tempVect * movementSpeed * Time.deltaTime;
        rb.MovePosition(transform.position + tempVect);
    }

    public void FireLaser()
    {
        Instantiate(laserPrefab, transform.position, Quaternion.identity);
    }

    public float ChargeShields(float amount = 0.02f)
    {
        shieldLevels += amount;
        if (shieldLevels > 1) shieldLevels = 1;

        return shieldLevels;
    }

    void UpdateShields()
    {
        shieldLevels -= shieldLossOverTime * Time.deltaTime;
        if (shieldLevels < 0) shieldLevels = 0;

        shield.GetComponent<Light2D>().intensity = shieldLevels;
        shield.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, shieldLevels);
    }

    void asteroidHit(float damage)
    {
        TakeDamage(damage);
    }

    void BlogUp()
    {
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
