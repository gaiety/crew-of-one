﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spaceExplosion : MonoBehaviour
{
    void Update()
    {
        StartCoroutine(DestroyAfterAnimation());
    }

    IEnumerator DestroyAfterAnimation()
    {
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }
}
