﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class character : MonoBehaviour
{
    public GameObject idle;
    public GameObject run;
    public GameObject interact;
    public float movementSpeed;
    Rigidbody2D rb;
    GameObject latestTerminal;
    bool isAtTerminal = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        SetupAnimations();
    }

    void Update()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        Vector3 tempVect = new Vector3(h, v, 0);
        tempVect = tempVect.normalized * movementSpeed * Time.deltaTime;
        rb.MovePosition(transform.position + tempVect);

        AnimationsFromMovement(h, v);
        ToggleTerminal(h, v);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        isAtTerminal = other.CompareTag("terminal");
        latestTerminal = other.gameObject;
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        isAtTerminal = !collider.CompareTag("terminal");
    }

    void SetupAnimations()
    {
        idle.SetActive(true);
        run.SetActive(false);
    }

    void AnimationsFromMovement(float h, float v)
    {
        string desiredAnimation = h == 0 && v == 0 ? "idle" : "run";
        SetAnimation(desiredAnimation);
        FlipAnimations(h);
    }

    void SetAnimation(string animationName = "idle")
    {
        idle.SetActive(animationName == "idle" && !isAtTerminal);
        run.SetActive(animationName == "run");
        interact.SetActive(animationName == "idle" && isAtTerminal);
    }

    void FlipAnimations(float h)
    {
        if (h == 0) return;
        bool flip = h < 0;

        idle.GetComponent<SpriteRenderer>().flipX = flip;
        run.GetComponent<SpriteRenderer>().flipX = flip;
    }

    void ToggleTerminal(float h, float v)
    {
        bool isStandingStill = h == 0 && v == 0;

        if (latestTerminal) latestTerminal.GetComponent<terminal>().screen.SetActive(isStandingStill && isAtTerminal);
    }
}
