﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroid : MonoBehaviour
{
    public Transform explosionPrefab;
    public float destroyWhenY = -10.0f;
    public float speed = 0.05f;
    public float damage = 0.25f;
    Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (!shouldDestroySelf()) moveDown();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "ship")
        {
            collider.gameObject.SendMessage("asteroidHit", damage);
            blowUp();
        }
    }

    bool shouldDestroySelf()
    {
        if (transform.position.y <= destroyWhenY)
        {
            Destroy(gameObject);
            return true;
        }
        return false;
    }

    void moveDown()
    {
        Vector3 tempVect = new Vector3(0, -1, 0);
        tempVect = tempVect * speed * Time.deltaTime;
        rb.MovePosition(transform.position + tempVect);
    }

    void laserHit()
    {
        blowUp();
    }

    void blowUp()
    {
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
