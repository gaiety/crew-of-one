DithArt's Sci-Fi Tileset 2 - Med Bay
https://dithart.itch.io/ditharts-sci-fi-tileset2

SPACE by VECTORPIXELSTAR
https://vectorpixelstar.itch.io/space

Warped Space Marine by ansimuz
https://ansimuz.itch.io/warped-space-marine

Spaceships by wuhu
https://opengameart.org/content/spaceships-1

UI Sci-Fi Nesia #03 by Wenrexa
https://wenrexa.itch.io/nesia03

Asteroid by Warspawn
https://opengameart.org/content/asteroid

Zelta-Six by StudioTypo
https://www.1001fonts.com/zelta-six-demo-font.html

Ring Explosion by BenHickling
https://opengameart.org/content/ring-explosion